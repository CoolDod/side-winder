{
    "id": "5bd098ec-e66f-4b3b-b27a-08a0b3300da1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRunning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd9f9ee8-443c-4f4e-a6f2-d3cf00669b41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bd098ec-e66f-4b3b-b27a-08a0b3300da1",
            "compositeImage": {
                "id": "0866414d-8a90-4183-b714-d438235d0bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9f9ee8-443c-4f4e-a6f2-d3cf00669b41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ed85e0-226b-4b3f-8997-5baad97cf9a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9f9ee8-443c-4f4e-a6f2-d3cf00669b41",
                    "LayerId": "9ef3618d-43b5-40bc-8c1d-3e8927ef33e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ef3618d-43b5-40bc-8c1d-3e8927ef33e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bd098ec-e66f-4b3b-b27a-08a0b3300da1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}