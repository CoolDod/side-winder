{
    "id": "d1252914-1123-4f74-b1cb-35592154c936",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 8,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ca5f77a-837b-41d5-bc44-a8270cdfa6fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1252914-1123-4f74-b1cb-35592154c936",
            "compositeImage": {
                "id": "6b08cfe8-4844-4636-808b-cb97b00ee925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca5f77a-837b-41d5-bc44-a8270cdfa6fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff25cb6-a80a-4984-a7e9-e2d93445e064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca5f77a-837b-41d5-bc44-a8270cdfa6fd",
                    "LayerId": "e84d0f98-52f0-4663-b3bf-81be7757370c"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 8,
    "layers": [
        {
            "id": "e84d0f98-52f0-4663-b3bf-81be7757370c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1252914-1123-4f74-b1cb-35592154c936",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 4
}