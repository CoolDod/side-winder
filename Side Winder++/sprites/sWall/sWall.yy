{
    "id": "f694f849-5cd2-4992-841c-ca399c8bcd11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff004c13-c87f-4fd0-8f52-fbe7dc68ecb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "4074307d-b343-4771-979d-dbdcaf489aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff004c13-c87f-4fd0-8f52-fbe7dc68ecb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907ba6af-cb56-4015-bafb-3b7cc8644a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff004c13-c87f-4fd0-8f52-fbe7dc68ecb8",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "7b36b668-6fcf-4b5b-8ca1-bbf73a893c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "823bb971-4311-42ab-a460-47beffa5d132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b36b668-6fcf-4b5b-8ca1-bbf73a893c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "259e09a0-3c19-4188-9540-33d42f7c8af2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b36b668-6fcf-4b5b-8ca1-bbf73a893c6e",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "724b6b32-c3eb-4bfb-934e-c51b617d79f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "0169edff-6a30-4e9d-b0ad-b5e34f775ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "724b6b32-c3eb-4bfb-934e-c51b617d79f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017f5712-4b36-4834-b3bc-4c8c5c030b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "724b6b32-c3eb-4bfb-934e-c51b617d79f3",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "fbff605d-ee0f-4c91-a2c2-468b4b2c0430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "fcae0bf3-d72d-4cbe-9f1b-3b70c57bf211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbff605d-ee0f-4c91-a2c2-468b4b2c0430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a377e72-3c6e-4f1b-a4c2-9658f8c00e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbff605d-ee0f-4c91-a2c2-468b4b2c0430",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "19c5e8ae-e6d3-4427-b151-5bb88ca3d30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "16b2c0e7-d12a-4e52-b44e-3a88ae0906f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c5e8ae-e6d3-4427-b151-5bb88ca3d30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8077c162-b88b-4ebc-b64f-8f603f722ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c5e8ae-e6d3-4427-b151-5bb88ca3d30e",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "b5fd0169-2263-4758-9c27-bef7a06f4404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "bba2a5ef-03fc-48d1-b144-d4fdf54276aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fd0169-2263-4758-9c27-bef7a06f4404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512a4299-fff9-4f66-ad9d-65aa1f90e239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fd0169-2263-4758-9c27-bef7a06f4404",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "678fd891-7a11-450c-bd96-acb2ca2aa615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "d825af88-8abb-41cf-8a8d-d76348649f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "678fd891-7a11-450c-bd96-acb2ca2aa615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96812c50-6f0e-43e6-9b98-2db4e1b56dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "678fd891-7a11-450c-bd96-acb2ca2aa615",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "4539074f-fe66-4953-b26a-db6e4f902b74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "6391e675-7ab3-4e4d-ae1f-18b945312321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4539074f-fe66-4953-b26a-db6e4f902b74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb9f0dc-b6b0-4405-afe6-f94b483b3560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4539074f-fe66-4953-b26a-db6e4f902b74",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "ad591a0b-41db-4296-b8f4-4b57a1a633aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "f956a860-753e-416c-b56a-dce79f020e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad591a0b-41db-4296-b8f4-4b57a1a633aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48cabfdc-7b54-432f-8e09-01a4f37ff3f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad591a0b-41db-4296-b8f4-4b57a1a633aa",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "2c3ddf04-8ee7-48c5-8b58-ac377c2be2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "8d44295f-3525-4a96-b38d-767fd4ccbc50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3ddf04-8ee7-48c5-8b58-ac377c2be2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3c761e-0478-42cf-b91f-7c1924327677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3ddf04-8ee7-48c5-8b58-ac377c2be2cd",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "bed04126-39d9-4087-a087-3cd21f4a35ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "a745049a-baf1-466e-8b23-2a8cd57d1d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed04126-39d9-4087-a087-3cd21f4a35ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1588ec5-8526-4e17-8327-fe72b56ce32b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed04126-39d9-4087-a087-3cd21f4a35ee",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "deaa08f6-7713-45c9-bee6-f394a4b154d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "eba70b18-f33b-42ca-a3df-636c1c2d9b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deaa08f6-7713-45c9-bee6-f394a4b154d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba047e0a-368d-42f7-9f84-f42ac91d17f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deaa08f6-7713-45c9-bee6-f394a4b154d5",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "6c9bce00-2639-48d1-8962-8168f57e8d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "761469ac-f6d6-442c-8d56-19166dc2f4c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9bce00-2639-48d1-8962-8168f57e8d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8d0ca3-8ada-4025-8d7b-1bd7d451e6ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9bce00-2639-48d1-8962-8168f57e8d46",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "752b16c0-9abd-4ed8-bb2d-02549e3888aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "eff69e59-9bf0-4513-8fd2-43a1ebda8b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "752b16c0-9abd-4ed8-bb2d-02549e3888aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9215375a-7e26-44a2-9a4f-4092d56d0f99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "752b16c0-9abd-4ed8-bb2d-02549e3888aa",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "8eac264d-0342-4f90-907a-a524e281ffb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "f26bfcbd-3255-4e8a-85d7-cfb54f1b9ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eac264d-0342-4f90-907a-a524e281ffb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e621d3-7763-4b92-8391-b25ade2cf92c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eac264d-0342-4f90-907a-a524e281ffb0",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "8f5abc78-e10d-4969-aada-e28ec36e2107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "9259f179-500f-47b8-aa5b-ad447c0964ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5abc78-e10d-4969-aada-e28ec36e2107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ca73e6-53ad-4c21-b106-75e11b12cd7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5abc78-e10d-4969-aada-e28ec36e2107",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "083efa01-ad11-49af-a7e4-cc65074c86a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "853a74a1-d811-424d-bd51-58bcfd426487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083efa01-ad11-49af-a7e4-cc65074c86a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30aa1533-67f7-413d-95b2-80dfb520baa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083efa01-ad11-49af-a7e4-cc65074c86a6",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "22cbb129-5f8b-4e50-84f3-620d0f4939fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "748fd7be-0314-41ac-9085-6fd77044d4e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22cbb129-5f8b-4e50-84f3-620d0f4939fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae6d063-a31f-4c9c-b731-4de711e05af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22cbb129-5f8b-4e50-84f3-620d0f4939fa",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "c3adc4f9-0331-4d0f-896d-3960c719037b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "8243370e-7af3-48a5-9c1b-e70f0e30ab49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3adc4f9-0331-4d0f-896d-3960c719037b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7cff21-f4d1-4d4b-a39f-8ec666e63ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3adc4f9-0331-4d0f-896d-3960c719037b",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "b394e466-eee1-4884-8342-387a567451ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "3bafa857-dd57-4781-b9a8-89db9d3c5997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b394e466-eee1-4884-8342-387a567451ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d35847-652c-4bf7-9db6-05e91a573f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b394e466-eee1-4884-8342-387a567451ae",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "99f1494b-d5d1-415a-a6e8-7cc3861c55e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "df4e64e8-489b-418f-ad49-9a188990f848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f1494b-d5d1-415a-a6e8-7cc3861c55e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2280324b-2342-43a4-afff-deef6e13e884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f1494b-d5d1-415a-a6e8-7cc3861c55e3",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "3691c95c-2443-4a63-835d-87511e031e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "9445496f-3020-4bd3-a4df-4e42a5a43bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3691c95c-2443-4a63-835d-87511e031e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952ff926-cf0a-48a4-964c-5878f4205416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3691c95c-2443-4a63-835d-87511e031e82",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "691cb544-31b3-4873-994c-819da9874ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "41cfc441-9211-47bb-ad11-f1fabbe8c26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691cb544-31b3-4873-994c-819da9874ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94230ace-1792-4c53-a03a-9bb1052a0c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691cb544-31b3-4873-994c-819da9874ce1",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "cd08d05e-356f-46cd-be4d-8885c1c44713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "0427f471-1588-4bc8-9cf1-02ddb2b542c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd08d05e-356f-46cd-be4d-8885c1c44713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5b85e3-6b0a-47d3-809c-ce856f5e6aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd08d05e-356f-46cd-be4d-8885c1c44713",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "2a84fdf8-4dc1-479d-a679-32c2e801c08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "ef06e748-577d-47d9-be20-1a656548a451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a84fdf8-4dc1-479d-a679-32c2e801c08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0216f66-30ae-4672-9990-16019556248a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a84fdf8-4dc1-479d-a679-32c2e801c08a",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "6258d41c-bbea-45b4-92bb-572c0338023d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "17b646ff-aaa0-4e6c-b48b-bd5ff89901e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6258d41c-bbea-45b4-92bb-572c0338023d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182aa53c-6f35-4259-8aa4-b17383299d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6258d41c-bbea-45b4-92bb-572c0338023d",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "939e160b-db17-4377-95c6-7239ff23948c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "3c3a8f74-82aa-4b13-9802-91af036e0072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "939e160b-db17-4377-95c6-7239ff23948c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "911e5ab0-bd9b-400d-96dc-b9cc199c4981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "939e160b-db17-4377-95c6-7239ff23948c",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "448c41fc-b4de-4dd0-a454-4c19812deb0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "58c2e770-7d07-45e4-995f-b19c9a6e72ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448c41fc-b4de-4dd0-a454-4c19812deb0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692e19f9-7c92-417e-ab37-7ac9a41b6292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448c41fc-b4de-4dd0-a454-4c19812deb0c",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "b67a5956-3f7c-42b3-a3f9-55e4aed0923a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "d480b6e1-2e26-425b-8cbe-4883e70fe3c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b67a5956-3f7c-42b3-a3f9-55e4aed0923a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e5ff5f-6910-432c-b3d9-fb663e270edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b67a5956-3f7c-42b3-a3f9-55e4aed0923a",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "095fadd8-cf2b-4506-9fec-0e43729183d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "f2edd453-dd8a-474e-9172-7d78671bfa92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095fadd8-cf2b-4506-9fec-0e43729183d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e59c22-9a87-4a12-9f3d-0fdb2c3b6d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095fadd8-cf2b-4506-9fec-0e43729183d8",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "5c7fa06f-fdd0-4c4e-930b-bc03ab68666d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "76a42651-0641-49dc-bb19-db0c6be31b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7fa06f-fdd0-4c4e-930b-bc03ab68666d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8f2748-c344-40aa-bf2c-d253daf36c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7fa06f-fdd0-4c4e-930b-bc03ab68666d",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "e69d6ac4-a761-461c-ad9e-8744d17b72ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "2ce1ff4d-263f-421d-ac42-2f8ed4858349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69d6ac4-a761-461c-ad9e-8744d17b72ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e0e8626-163b-43dc-8933-b727b35284eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69d6ac4-a761-461c-ad9e-8744d17b72ff",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "daa169ea-9cb2-4522-9a47-491b637710e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "c2ba0520-4944-47d9-b2a0-d02098961750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa169ea-9cb2-4522-9a47-491b637710e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7632526-739b-4fa7-bfb9-e855f5f52d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa169ea-9cb2-4522-9a47-491b637710e6",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "ee788aa1-8203-465d-9b94-4fd44e0bcc1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "1058eb1d-1901-4691-840e-fb8c8532e934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee788aa1-8203-465d-9b94-4fd44e0bcc1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "171509ee-3998-418b-9fd9-f401080c41e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee788aa1-8203-465d-9b94-4fd44e0bcc1a",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "4afd4ffd-d97c-40c9-adce-61e17be464e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "17b3fa91-90f4-4eba-8cb1-1b8b4a0262d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4afd4ffd-d97c-40c9-adce-61e17be464e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b4cee0-25dd-4b70-bf63-beb37c78711c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4afd4ffd-d97c-40c9-adce-61e17be464e7",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "74f59720-52c9-4800-af1a-d577d19dcea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "6cb7b0de-5a97-4c51-b6fa-c1aa0846e421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f59720-52c9-4800-af1a-d577d19dcea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e52fd8f1-b7f4-49b0-9cef-829a7c149998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f59720-52c9-4800-af1a-d577d19dcea8",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "f93eab0a-1ba1-4c40-a6aa-cc87fcd8875c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "e0201350-f803-438f-a537-ac06fb144057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f93eab0a-1ba1-4c40-a6aa-cc87fcd8875c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1022e237-d9f0-4e2e-aa55-2ab0880e1a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f93eab0a-1ba1-4c40-a6aa-cc87fcd8875c",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "271ff853-e72e-4cd5-b802-0ae6cb99553b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "178279b9-bd2f-4d45-be32-55f145f64f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271ff853-e72e-4cd5-b802-0ae6cb99553b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5e4043-a266-451d-b188-3dbe755d97e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271ff853-e72e-4cd5-b802-0ae6cb99553b",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "4dfebdf6-5054-4d20-9ab2-8a24de4582cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "6a758ffe-0e0b-4df5-8f58-cba38a8cba2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dfebdf6-5054-4d20-9ab2-8a24de4582cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e27f798-846d-42d8-bac6-e59fdf918785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dfebdf6-5054-4d20-9ab2-8a24de4582cb",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "b990b11d-267a-4c85-8b2b-7a4688080f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "b4d732a7-eb8b-4a35-b8cf-005df2eda173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b990b11d-267a-4c85-8b2b-7a4688080f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25f83a0-13b1-487d-adea-4a7c00e916db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b990b11d-267a-4c85-8b2b-7a4688080f69",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "2a8002b5-d3e4-4a76-96a6-c0d82adc5593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "f0c67315-0c48-43cf-a01a-5734501ee772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a8002b5-d3e4-4a76-96a6-c0d82adc5593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640a6905-ca2f-4287-91ca-a5786177ee70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a8002b5-d3e4-4a76-96a6-c0d82adc5593",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "973ab7e9-fd45-4f4c-a1f4-561af9828ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "93d8223b-7c8f-4d83-af9d-8c5bb3fb8dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973ab7e9-fd45-4f4c-a1f4-561af9828ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "857eb54a-a479-4433-bcfb-612acceebde2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973ab7e9-fd45-4f4c-a1f4-561af9828ef6",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "0ce22b51-bd69-465d-8fec-7154426b5a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "325b86e0-1085-47a0-8e4c-7f91d57d37d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce22b51-bd69-465d-8fec-7154426b5a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7fc9a5-5061-430a-a2ff-c5cd7555096e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce22b51-bd69-465d-8fec-7154426b5a1e",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "a807d7cc-7218-4529-8213-daadc92c7923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "6c839bec-20d0-4c18-a7b6-bb3e1cfb5611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a807d7cc-7218-4529-8213-daadc92c7923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddfa2d3d-905f-4c48-81b8-ee67363a6e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a807d7cc-7218-4529-8213-daadc92c7923",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "b963205b-0df1-4c01-adc2-9d6031d31635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "8dcff9b2-157d-4056-b9af-1723d480dd2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b963205b-0df1-4c01-adc2-9d6031d31635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde0c3dc-f3c4-459f-b8ce-ace9b2d0b834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b963205b-0df1-4c01-adc2-9d6031d31635",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "41853e0c-c8b0-437d-9398-8cbbca14e94c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "b2c8d449-ff0d-4d6d-b426-ba1cbaf42534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41853e0c-c8b0-437d-9398-8cbbca14e94c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd0a502-417c-4023-88d4-ae0e3a027fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41853e0c-c8b0-437d-9398-8cbbca14e94c",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "7c7ba5e1-8679-4f2a-b1d1-fe4d5771165d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "f84031ab-27c5-464c-ba48-18d2628f43b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c7ba5e1-8679-4f2a-b1d1-fe4d5771165d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4366471e-cdf0-41aa-aa51-41ed8f6fe344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c7ba5e1-8679-4f2a-b1d1-fe4d5771165d",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        },
        {
            "id": "99724e50-35b1-4925-b2d9-d32f48f34d71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "compositeImage": {
                "id": "e0bedaf0-7262-463b-a47d-34ab32e376b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99724e50-35b1-4925-b2d9-d32f48f34d71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebef9a61-3a63-4fa6-81d4-38ee00fc88d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99724e50-35b1-4925-b2d9-d32f48f34d71",
                    "LayerId": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ad233f30-0b5a-47e0-91fc-7ce3f1eaa6ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}