{
    "id": "094fe87a-304c-4ec4-bc05-dad8837de148",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6681c379-5a30-4458-81c3-4c6a9b4dfce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "094fe87a-304c-4ec4-bc05-dad8837de148",
            "compositeImage": {
                "id": "beaa0f68-7f90-474f-a41f-5f1df1a3e4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6681c379-5a30-4458-81c3-4c6a9b4dfce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ace952-8557-4230-af97-3580f46c93a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6681c379-5a30-4458-81c3-4c6a9b4dfce0",
                    "LayerId": "90a8702a-c5dd-45b8-a04c-fe66bebd52e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "90a8702a-c5dd-45b8-a04c-fe66bebd52e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "094fe87a-304c-4ec4-bc05-dad8837de148",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}