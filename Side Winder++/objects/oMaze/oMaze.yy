{
    "id": "c305c4c9-5681-47bb-aec2-c472406d5df4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMaze",
    "eventList": [
        {
            "id": "120cd404-dfb3-4f56-b393-370271f6e1a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c305c4c9-5681-47bb-aec2-c472406d5df4"
        },
        {
            "id": "d6e9e008-1635-4346-a3e0-fc2ef34f1172",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c305c4c9-5681-47bb-aec2-c472406d5df4"
        },
        {
            "id": "1c538539-bcd5-4a9d-9931-a037e4aa9a36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c305c4c9-5681-47bb-aec2-c472406d5df4"
        },
        {
            "id": "6c7266d4-84bd-4d72-b23f-3512eeee8d6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c305c4c9-5681-47bb-aec2-c472406d5df4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}