/// @desc

tile_types();

xwalls = 8;
ywalls = 8;

wall_width = sprite_get_width(sWall);
wall_height = sprite_get_height(sWall);

cell_width = wall_width * xwalls;
cell_height = wall_height * ywalls;

xcells = floor(room_width / cell_width);
ycells = floor(room_height / cell_height);

mw = xcells * cell_width;
mh = ycells * cell_height;

mx = (room_width - mw) / 2;
my = (room_height - mh) / 2;

cell_map = array_create_2d(xcells, ycells, noone);

cell_num = 0;
cell_max = xcells * ycells;

start_x_ind = 0;
start_y_ind = 0;

start_x = start_x_ind * cell_width + cell_width / 2;
start_y = start_y_ind * cell_height + cell_height / 2;

tile_layer = layer_tilemap_get_id(layer_get_id("tlFloor"));
tilemap_x(tile_layer, mx);
tilemap_y(tile_layer, my);
tilemap_set_width(tile_layer, mw);
tilemap_set_height(tile_layer, mh);

generate_fast = false;

if (generate_fast)
	aldous_broder_fast();
else {
	aldous_broder_init();
	instance_create_layer(room_width / 2, room_height / 2, "iControllers", oCamera);
}