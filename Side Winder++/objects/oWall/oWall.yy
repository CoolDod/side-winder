{
    "id": "5ecd63f5-da3a-4b86-acb9-5542a2a3253c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWall",
    "eventList": [
        {
            "id": "daa4808e-d3ee-4500-9dfa-0ed240264442",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ecd63f5-da3a-4b86-acb9-5542a2a3253c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f694f849-5cd2-4992-841c-ca399c8bcd11",
    "visible": true
}