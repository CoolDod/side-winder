/// @desc

// Transistion to new state
if (state != new_state) {
	// Calculate new coordinates
	x = lerp(x, new_state[? "x"], lerp_spd);
	y = lerp(y, new_state[? "y"], lerp_spd);
	
	// Calculate new view size
	width = lerp(width, new_state[? "width"], lerp_spd);
	height = lerp(height, new_state[? "height"], lerp_spd);
	
	// Calculate new angle
	angle = lerp(angle, new_state[? "angle"], lerp_spd);
	
	// Calculate new zoom amount
	zoom = lerp(zoom, new_state[? "zoom"], lerp_spd);
	
	// Set projection
	camera_set_proj(camera, width, height, zoom);
	// Set view
	camera_set_view(camera, x, y, angle);
	
	// Check if transition is complete
	var dist = abs(x - new_state[? "x"]);
	
	if (dist < lerp_margin) {
		// Set camera variables to new state's variables
		x = new_state[? "x"];
		y = new_state[? "y"];
		width = new_state[? "width"];
		height = new_state[? "height"];
		angle = new_state[? "angle"];
		zoom = new_state[? "zoom"];
		
		// Set projection
		camera_set_proj(camera, width, height, zoom);
		// Set view
		camera_set_view(camera, x, y, angle);
		
		// Set current state to new state
		state = new_state;
	}
}
else if (target != noone && (x != target.x || y != target.y)) {
	// Calculate new camera position
	x = lerp(x, target.x, lerp_spd);
	y = lerp(y, target.y, lerp_spd);
	
	// Set view
	camera_set_view(camera, x, y, angle);
}