/// @desc

width = room_width;
height = room_height;

angle = 0;
zoom  = 1;

camera = camera_setup(0, 0, 0, 0, 0, 1, 0, true);
target = noone;

camera_states[1] = camera_state_create(oMaze.start_x, oMaze.start_y, width, height, 0, 0.25, oPlayer);
camera_states[0] = camera_state_create(room_width / 2, room_height / 2, width, height, 0, 1, noone);

sInd = 0;
state = 0;
new_state = 0;
lerp_spd = 0.1;
lerp_deadzone = 0.05;
lerp_margin = 0;

camera_state_set(id, sInd, false);
