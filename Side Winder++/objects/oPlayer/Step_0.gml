/// @desc

#region GET CONTROLS
	var up = keyboard_check(ord("W"));
	var down = keyboard_check(ord("S"));
	var left = keyboard_check(ord("A"));
	var right = keyboard_check(ord("D"));
#endregion

#region MOVEMENT
	hdir = right - left;
	vdir = down - up;
	
	hspd = spd * hdir;
	vspd = spd * vdir;
	
	// Horizontal collision
	if (place_meeting(x + hspd, y, oWall)) {
		while (!place_meeting(x + hdir, y, oWall))
			x += hdir;
		
		hspd = 0;
	}
	
	// Vertical collision
	if (place_meeting(x, y + vspd, oWall)) {
		while (!place_meeting(x, y + vdir, oWall))
			y += vdir;
			
		vspd = 0;
	}
	
	x += hspd;
	y += vspd;
#endregion