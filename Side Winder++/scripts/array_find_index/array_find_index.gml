#region	SCRIPT DESCRIPTION
	/// @func array_find_index(array,value)
	/// @desc Returns the index the given value is found in the array
	/// @arg array
	/// @arg value
#endregion
#region DECLARE VARIABLES
	var _array = argument0;
	var _value = argument1;
#endregion


#region ARRAY FIND INDEX 1D
	for (var _i = 0; _i < array_length_1d(_array); _i++) {
		if (_value == _array[_i]) break;
	}
	
	return (_i);
#endregion