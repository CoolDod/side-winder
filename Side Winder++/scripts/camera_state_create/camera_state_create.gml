#region SCRIPT DESCRIPTION
	/// @func camera_state_create(x,y,width,height,angle,zoom,target)
	/// @desc Returns a data structure that represents a camera
	/// @arg {int} x The x coordinate of the centre of the camera
	/// @arg {int} y The y coordinate of the centre of the camera
	/// @arg {int} width The width of the camera view
	/// @arg {int} height The height of the camera view
	/// @arg {float} angle The rotational angle of the camera
	/// @arg {float} zoom The zoom amount of the camera view
	/// @arg {int} target The instance that the object is following, set to the keyword 'noone' if it shouldn't follow any instance
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
	var _w = argument2;
	var _h = argument3;
	var _rot = argument4;
	var _zoom = argument5;
	var _target = argument6;
#endregion


#region CAMERA STATE CREATE
	var _camera = ds_map_create();
	
	_camera[? "x"] = _x;
	_camera[? "y"] = _y;
	_camera[? "width"] = _w;
	_camera[? "height"] = _h;
	_camera[? "angle"] = _rot;
	_camera[? "zoom"] = _zoom;
	_camera[? "target"] = _target;
	_camera[? "projection"] = matrix_build_projection_ortho(_w * _zoom, _h * _zoom, 1.0, 32000.0);
	_camera[? "view"] = matrix_build_lookat(_x, _y, -10, _x, _y, 0, dsin(_rot), dcos(_rot), 0);
	
	return (_camera);
#endregion