#region SCRIPT DESCRIPTION
	/// @func wall_autotile_remove(x, y)
	/// @desc Removes the calling wall instance and alters all neighbouring tiles appropiately
	/// @arg {int} x
	/// @arg {int} y
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
#endregion


#region WALL AUTOTILE REMOVE
	var _inst = instance_position(_x, _y, oWall);

	// Get neighbouring walls
	var _neighbours = wall_get_neighbours(_inst);
	
	var _top = _neighbours[Directions.North];
	var _bottom = _neighbours[Directions.South];
	var _left = _neighbours[Directions.West];
	var _right = _neighbours[Directions.East];
	var _topleft = _neighbours[Directions.NorthWest];
	var _topright = _neighbours[Directions.NorthEast];
	var _bottomleft = _neighbours[Directions.SouthWest];
	var _bottomright = _neighbours[Directions.SouthEast];
	
	// Change top left
	// If there is a wall at the top left
	if (_topleft != noone) {
		// If wall has a corner at the bottom right, remove it
		if (_top != noone && _left != noone)
			wall_add_element(_topleft, TileElement.cornerBR);
	}
	
	// Change top right
	// If there is a wall at the top right
	if (_topright != noone) {
		// If wall has corner at the bottom left, remove it
		if (_top != noone && _right != noone)
			wall_add_element(_topright, TileElement.cornerBL);
	}
	
	// Change bottom left
	// If there is a wall at the bottom left
	if (_bottomleft != noone) {
		// If wall has corner at the top right, remove it
		if (_bottom != noone && _left != noone)
			wall_add_element(_bottomleft, TileElement.cornerTR);
	}
	
	// Change bottom right
	// If there is a wall at the bottom right
	if (_bottomright != noone) {
		// If wall has a corner at the top left, remove it
		if (_bottom != noone && _right != noone)
			wall_add_element(_bottomright, TileElement.cornerTL);
	}
	
	// Change top
	// If there is a wall at the top
	if (_top != noone) {
		// Remove bottom corners from top wall
		_top.elements = array_subtract(_top.elements, [TileElement.cornerBL, TileElement.cornerBR]);
		
		// Add bottom side to top wall
		_top.elements = array_add(_top.elements, [TileElement.sideB]);

		_top.image_index = wall_elements_get_index(_top.elements);
	}
	
	// Change bottom
	// If there is a wall at the bottom
	if (_bottom != noone) {
		// Remove top corners from bottom wall
		_bottom.elements = array_subtract(_bottom.elements, [TileElement.cornerTL, TileElement.cornerTR]);
		
		// Add a top side to the bottom wall
		_bottom.elements = array_add(_bottom.elements, [TileElement.sideT]);
		
		_bottom.image_index = wall_elements_get_index(_bottom.elements);
	}
	
	// Change left
	// If there is a wall at the left
	if (_left != noone) {
		// Remove right corners from left wall
		_left.elements = array_subtract(_left.elements, [TileElement.cornerBR, TileElement.cornerTR]);
		
		// Add right side to left wall
		_left.elements = array_add(_left.elements, [TileElement.sideR]);
			
		_left.image_index = wall_elements_get_index(_left.elements);
	}
	
	// Change right
	// If there is a wall at the right
	if (_right != noone) {
		// Remove left corner from right wall
		_right.elements = array_subtract(_right.elements, [TileElement.cornerBL, TileElement.cornerTL]);
		
		// Add left side to right wall
		_right.elements = array_add(_right.elements, [TileElement.sideL]);
		
		_right.image_index = wall_elements_get_index(_right.elements);
	}
	
	instance_destroy(_inst);
#endregion