#region SCRIPT DESCRIPTION
	/// @func cell_choose_direction(id,unvisited)
	/// @desc Randomly chooses an available direction from the given cell
	/// @arg {int} id The id of the cell
	/// @arg {bool} unvisted Whether or not to only include directions to neighbours that are unvisited
#endregion
#region DECLARE VARIABLES
	var _cell = argument0;
	var _unvisited = argument1;
#endregion


#region CELL CHOOSE DIRECTION
	var _x = _cell[0];
	var _y = _cell[1];

	var _dirs = ds_list_create();
	
	// Top
	if (_y > 0) {
		if (_unvisited) {
			if (oMaze.cell_map[_x, _y - 1] == noone)
				ds_list_add(_dirs, Directions.North);
		}
		else
			ds_list_add(_dirs, Directions.North);
	}
	// Bottom
	if (_y < oMaze.ycells - 2) {
		if (_unvisited) {
			if (oMaze.cell_map[_x, _y + 1] == noone)
				ds_list_add(_dirs, Directions.South);
		}
		else
			ds_list_add(_dirs, Directions.South);
	}
	// Left
	if (_x > 0) {
		if (_unvisited) {
			if (oMaze.cell_map[_x - 1, _y] == noone)
				ds_list_add(_dirs, Directions.West);
		}
		else
			ds_list_add(_dirs, Directions.West);
	}
	// Right
	if (_x < oMaze.xcells - 2) {
		if (_unvisited) {
			if (oMaze.cell_map[_x + 1, _y] == noone)
				ds_list_add(_dirs, Directions.East);
		}
		else
			ds_list_add(_dirs, Directions.East);
	}
	
	// Choose direction
	if (ds_list_size(_dirs) == 0)
		var _dir = -1;
	else
		var _dir = _dirs[| irandom(ds_list_size(_dirs) - 1)];
	
	// Destroy list
	ds_list_destroy(_dirs);
	
	// Return chosen direction
	return _dir;
#endregion