#region SCRIPT DESCRIPTION
	/// @func camera_get_viewport(camera)
	/// @desc Returns the viewport that the camera is assigned to or -1 if it isn't assigned
	/// @arg {real} camera
#endregion
#region DECLARE VARIABLES	
	var _camera = argument0;
#endregion


#region CAMERA GET VIEWPORT
	var _i = 0;
	repeat (8) {
		if (view_camera[_i] == _camera)
			return (_i);
		
		_i++;
	}
	
	return (-1);
#endregion