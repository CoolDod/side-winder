#region SCRIPT DESCRIPTION
	/// @func camera_set_position(camera,x,y,rot)
	/// @desc Sets the given camera's position to the given coordinates
	/// @arg {real} camera
	/// @arg {real} x
	/// @arg {real} y
	/// @arg {real} rot
#endregion
#region DECLARE VARIABLES
	var _camera = argument0;
	var _x = argument1;
	var _y = argument2;
	var _rot = argument3;
#endregion


#region CAMERA SET VIEW
	var _viewmat = matrix_build_lookat(_x, _y, -10, _x, _y, 0, dsin(_rot), dcos(_rot), 0);

	camera_set_view_mat(_camera, _viewmat);
#endregion