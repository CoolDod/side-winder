#region SCRIPT DESCRIPTION
	/// @func wall_get_neighbours(id)
	/// @desc Returns the ids of all neighbouring walls
	/// @arg {int} id The id of the wall to check the neighbours of
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
#endregion


#region WALL GET NEIGHBOURS
	var _neighbours;
	
	var _left = _id.x - _id.sprite_width;
	var _right = _id.x + _id.sprite_width;
	var _up = _id.y - _id.sprite_height;
	var _down = _id.y + _id.sprite_height;
	
	// North neighbour
	_neighbours[Directions.North] = instance_position(_id.x, _up, _id.object_index);
	// South neighbour
	_neighbours[Directions.South] = instance_position(_id.x, _down, _id.object_index);
	// East neighbour
	_neighbours[Directions.East] = instance_position(_right, _id.y, _id.object_index);
	// West neighbour
	_neighbours[Directions.West] = instance_position(_left, _id.y, _id.object_index);
	// North West neighbour
	_neighbours[Directions.NorthWest] = instance_position(_left, _up, _id.object_index);
	// North East neighbour
	_neighbours[Directions.NorthEast] = instance_position(_right, _up, _id.object_index);
	// South West neighbour
	_neighbours[Directions.SouthWest] = instance_position(_left, _down, _id.object_index);
	// South East neighbour
	_neighbours[Directions.SouthEast] = instance_position(_right, _down, _id.object_index);
	
	return _neighbours;
#endregion