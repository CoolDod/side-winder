#region SCRIPT DESCRIPTION
	/// @func draw_grid(x1,y1,x2,y2,cell_width,cell_height)
	/// @desc Draws a grid with the given dimensions starting at the given coordniates
	/// @arg {int} x1 The left-most x coordinate of the grid
	/// @arg {int} y1 The top-most y coordinate of the grid
	/// @arg {int} x2 The right-most x coordinate of the grid
	/// @arg {int} y2 The bottom-most y coordinate of the grid
	/// @arg {int} cell_width The width of a grid cell
	/// @arg {int} cell_height The height of a grid cell
#endregion
#region DECLARE VARIABLES
	var _x1 = argument0;
	var _y1 = argument1;
	var _x2 = argument2;
	var _y2 = argument3;
	var _cw = argument4;
	var _ch = argument5;
#endregion


#region DRAW GRID
	var _w = _x2 - _x1;
	var _h = _y2 - _y1;

	var _xcells = _w div _cw + 1;
	var _ycells = _h div _ch + 1;
	
	var _i = 0;
	repeat (_xcells) {
		var _lx = _x1 + _cw * _i;
		draw_line(_lx, _y1, _lx, _y2);
		
		++_i;
	}
	
	draw_line(_x2, _y1, _x2, _y2);
	
	var _j = 0;
	repeat (_ycells) {
		var _ly = _y1 + _ch * _j;
		draw_line(_x1, _ly, _x2, _ly);
			
		++_j;
	}
	
	draw_line(_x1, _y2, _x2, _y2);
#endregion