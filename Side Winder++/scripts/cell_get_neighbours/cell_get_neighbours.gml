#region SCRIPT DESCRIPTION
	/// @func cell_get_neighbours(x,y)
	/// @desc Returns a list of the neighbouring cells
	/// @arg {int} x The x index of the cell
	/// @arg {int} y The y index of the cell
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
#endregion


#region CELL GET NEIGHBOURS
	var _neighbours;
	
	// Get top
	if (_y > 0)
		_neighbours[0] = oMaze.cell_map[_x, _y - 1];
	else
		_neighbours[0] = noone;
	// Get bottom
	if (_y < oMaze.ycells - 1)
		_neighbours[1] = oMaze.cell_map[_x, _y + 1];
	else
		_neighbours[1] = noone;
	// Get left
	if (_x > 0)
		_neighbours[2] = oMaze.cell_map[_x - 1, _y];
	else
		_neighbours[2] = noone;
	// Get right
	if (_x < oMaze.xcells - 1)
		_neighbours[3] = oMaze.cell_map[_x + 1, _y];
	else
		_neighbours[3] = noone;
		
	return _neighbours;
#endregion