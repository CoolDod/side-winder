#region SCRIPT DESCRIPTION
	/// @func recursive_backtracker_init()
	/// @desc Initialises the recursive backtracker algorithm
#endregion


#region RECURSIVE BACKTRACKER INIT
	oMaze.maze_generating = true;
	cell_num = 0;
	
	rb_xindex = irandom(oMaze.xcells - 1);
	rb_yindex = irandom(oMaze.ycells - 1);
	
	rb_path = ds_list_create();
	ds_list_add(rb_path, [rb_xindex, rb_yindex]);
	
	maze_set_start(rb_xindex, rb_yindex);
	
	cell_create(rb_xindex, rb_yindex, false, false, false, false);
#endregion