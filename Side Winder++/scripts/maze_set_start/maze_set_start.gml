#region SCRIPT DESCRIPTION
	/// @func maze_set_start(x_cell,y_cell)
	/// @desc Sets the starting cell for the maze
	/// @arg {int} x_cell The x index of the starting cell
	/// @arg {int} y_cell The y index of the starting cell
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
#endregion


#region MAZE SET START
	oMaze.start_x_ind = _x;
	oMaze.start_y_ind = _y;
	
	oMaze.start_x = (_x + .5) * oMaze.cell_width;
	oMaze.start_y = (_y + .5) * oMaze.cell_height;
#endregion