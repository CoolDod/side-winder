#region SCRIPT DESCRIPTION
	/// @func camera_state_set(id,state,lerp,{lerp_spd})
	/// @desc Sets a camera to a given state
	/// @arg {int} id The id of the camera
	/// @arg {int} state The id of the state
	/// @arg {bool} lerp Whether to smoothly transition to the next camera state or to 'jump' to it
	/// @arg {float} {lerp_spd} (optional) The speed at which the camera transitions to a new state
#endregion
#region DECLARE VARIABLES
	var _id = argument[0];
	var _state = argument[1];
	var _lerp = argument[2];
	
	if (argument_count > 3)
		var _lerp_spd = argument[3];
#endregion


#region CAMERA STATE SET
	_id.sInd = _state;
	_state = _id.camera_states[_state];

	if (!_lerp) {
		// Store new camera state
		_id.state = _state;
	
		// Set projection
		camera_set_proj_mat(_id.camera, _state[? "projection"]);
	
		// Set view
		camera_set_view_mat(_id.camera, _state[? "view"]);
	}
	else {
		// Get distance between current and new camera positions
		var _dist = abs(_id.x - _state[? "x"]);
		// Get lerp margin
		_id.lerp_margin = _dist * _id.lerp_deadzone;
		
		// Set transition speed
		if (argument_count > 3)
			_id.lerp_spd = _lerp_spd;
	}
	
	// Store new camera state
	_id.new_state = _state;
	
	// Set target
	_id.target = _state[? "target"];
#endregion