#region SCRIPT DESCRIPTION
	/// @func array_contains(array,value)
	/// @desc Checks if the given array contains the given value
	/// @arg {real} array
	/// @arg {real} value
#endregion
#region	DECLARE VARIABLES
	var _array = argument0;
	var _value = argument1;
#endregion


#region ARRAY CONTAINS 1D
	var _contains = false;
	
	for (var _i = 0; _i < array_length_1d(_array); _i++) {
		if (_value == _array[_i]) {
			_contains = true;
			break;
		}
	}
	
	return (_contains);
#endregion