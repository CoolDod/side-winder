#region SCRIPT DESCRIPTION
	/// @func aldous_broder_fast()
	/// @desc Runs the aldous-broder algorithm in one step
#endregion


#region ALDOUS BRODER FAST
	aldous_broder_init();
	
	while (oMaze.cell_num < oMaze.cell_max - 1)
		aldous_broder_step();
#endregion