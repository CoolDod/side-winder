#region SCRIPT DESCRIPTION
	/// @func tile_get_elements(tilemap_element_id,cell_x,cell_y)
	/// @desc Returns an array of the elements in the given tile
	/// @arg {int} tilemap_element_id The id of the tilemap
	/// @arg {int} cell_x The x index of the tile
	/// @arg {int} cell_y The y index of the tile
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
	var _cx = argument1;
	var _cy = argument2;
#endregion


#region TILE GET ELEMENTS
	var _tile = tilemap_get(_id, _cx, _cy);
	var _elements;

	for (var _i = 0; _i < array_length_1d(global.tileElements); ++_i) {
		var _array = global.tileElements[_i];
		
		for (var _j = 0; _j < array_length_1d(_array); ++_i) {
			if (_tile == _array[_j]) {
				array_add(_elements, [_i]);
				
				break;
			}
		}
	}
	
	return _elements;
#endregion