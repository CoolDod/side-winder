#region SCRIPT DESCRIPTION
	/// @func wall_remove_element(id,element)
	/// @desc Removes the given element from the given wall
	/// @arg {int} id The id of the wall
	/// @arg {int} element The tile element to remove
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
	var _element = argument1;
#endregion


#region WALL REMOVE ELEMENT
	_id.elements = array_subtract(_id.elements, [_element]);
	_id.image_index = wall_elements_get_index(_id.elements);
#endregion