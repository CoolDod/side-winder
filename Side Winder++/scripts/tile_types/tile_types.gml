// Tile elements
enum TileElement {
	sideT,
	sideB,
	sideL,
	sideR,
	cornerTL,
	cornerTR,
	cornerBL,
	cornerBR
}

// Tile neighbours
enum Directions {
	North,
	South,
	East,
	West,
	NorthWest,
	NorthEast,
	SouthWest,
	SouthEast
}

// Sides
global.topSides = 0;
global.topSides = array_add(global.topSides, [21, 22, 23, 24, 34, 35, 36, 37, 38, 43, 44, 46, 47]);

global.bottomSides = 0;
global.bottomSides = array_add(global.bottomSides, [29, 30, 31, 32, 34, 39, 40, 41, 42, 44, 45, 46, 47]);

global.leftSides = 0;
global.leftSides = array_add(global.leftSides, [17, 18, 19, 20, 33, 35, 36, 41, 42, 43, 44, 45, 47]);

global.rightSides = 0;
global.rightSides = array_add(global.rightSides, [25, 26, 27, 28, 33, 37, 38, 39, 40, 43, 45, 46, 47]);

// Corners
global.topleftCorners = 0;
global.topleftCorners = array_add(global.topleftCorners, [2, 4, 6, 8, 10, 12, 14, 16, 27, 28, 30, 32, 40]);

global.toprightCorners = 0;
global.toprightCorners = array_add(global.toprightCorners, [3, 4, 7, 8, 11, 12, 15, 16, 18, 20, 31, 32, 42]);

global.bottomleftCorners = 0;
global.bottomleftCorners = array_add(global.bottomleftCorners, [9, 10, 11, 12, 13, 14, 15, 16, 23, 24, 26, 28, 38]);

global.bottomrightCorners = 0;
global.bottomrightCorners = array_add(global.bottomrightCorners, [5, 6, 7, 8, 13, 14, 15, 16, 19, 20, 22, 24, 36]);


global.tileElements = 0;
global.tileElements = array_add(global.tileElements, [global.topSides, global.bottomSides, global.leftSides, global.rightSides, global.topleftCorners, global.toprightCorners, global.bottomleftCorners, global.bottomrightCorners]);
