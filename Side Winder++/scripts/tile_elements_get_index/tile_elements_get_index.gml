#region SCRIPT DESCRIPTION
	/// @func tile_elements_get_index([tile_elements])
	/// @desc Returns the tiledata that contains all of the given tile elements
	/// @arg {array} [tile_elements] The elements of the tile
#endregion
#region DECLARE VARIABLES
	var _elements = argument0;
#endregion


#region TILE ELEMENTS GET INDEX
	// Get element arrays
	for(var _i = 0; _i < array_length_1d(_elements); ++_i)
		_elements[_i] = global.tileElements[_elements[_i]];
	
	// Get element arrays not in tile
	var _nonElements = array_subtract(global.tileElements, _elements);
	
	var _shared = ds_list_create();	// Tiles shared by element arrays
	
	// If more than 1 element arrays
	if (array_length_1d(_elements) > 1) {
		var _a1 = _elements[0];
		var _a2 = _elements[1];
	
		for (_i = 0; _i < array_length_1d(_a1); ++_i) {
			for (var _j = 0; _j < array_length_1d(_a2); ++_j) {
				if (_a1[_i] == _a2[_j])
					ds_list_add(_shared, _a1[_i]);
			}
		}
		
		for (_i = 2; _i < array_length_1d(_elements); ++_i) {
			_a1 = _elements[_i];
		
			for (var _j = ds_list_size(_shared) - 1; _j >= 0; --_j) {
				var _contains = false;
			
				for (var _k = 0; _k < array_length_1d(_a1); ++_k) {
					if (_shared[_j] == _a1[_k]) {
						_contains = true;
						break;
					}
				}
			
				if (!_contains)
					ds_list_delete(_shared, _j);
			}
		}
	}
	// If only 1 element array
	else {
		var _a = _elements[0];
		
		for (var _i = 0; _i < array_length_1d(_a); ++_i)
			_shared[| _i] = _a[_i];
	}
	
	// Remove tiles that are in non-elements
	for (var _i = 0; _i < array_length_1d(_nonElements); ++_i) {
		var _a = _nonElements[_i];
		
		for (var _j = ds_list_size(_shared); _j >= 0; --_j) {
			for (var _k = 0; _k < array_length_1d(_a); ++_k) {
				if (_shared[| _j] == _k)
					ds_list_delete(_shared, _j);
			}
		}
	}
	
	// Return tile index
	return (_shared[| 0]);
#endregion