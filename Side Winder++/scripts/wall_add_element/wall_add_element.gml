#region SCRIPT DESCRIPTION
	/// @func wall_add_element(id,element)
	/// @desc Adds the given element to the given wall
	/// @arg {int} id The id of the wall
	/// @arg {int} element The element to add
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
	var _element = argument1;
#endregion


#region	WALL ADD ELEMENT
	_id.elements = array_add(_id.elements, [_element]);
	_id.image_index = wall_elements_get_index(_id.elements);
#endregion