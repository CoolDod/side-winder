#region SCRIPT DESCRIPTION
	/// @func aldous_broder_draw()
	/// @desc Draws highlighting for the aldous broder algorithm
#endregion


#region ALDOUS BRODER DRAW
	var _cx = ab_xindex * oMaze.cell_width;
	var _cy = ab_yindex * oMaze.cell_height;

	draw_set_color(c_red);

	draw_set_alpha(0.5);
	draw_rectangle(_cx, _cy, _cx + oMaze.cell_width, _cy + oMaze.cell_height, false);

	draw_set_alpha(1);
	draw_rectangle(_cx, _cy, _cx + oMaze.cell_width, _cy + oMaze.cell_height, true);

	draw_set_color(-1);
#endregion