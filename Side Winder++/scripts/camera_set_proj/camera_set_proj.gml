#region SCRIPT DESCRIPTION
	/// @func camera_set_proj(camera,width,height,zoom)
	/// @desc Sets the given camera's projection matrix
	/// @arg {real} camera
	/// @arg {real} width
	/// @arg {real} height
	/// @arg {real} zoom
#endregion
#region DECLARE VARIABLES
	var _camera = argument0;
	var _w = argument1;
	var _h = argument2;
	var _zoom = argument3;
#endregion


#region CAMERA SET PROJ
	var _projmat = matrix_build_projection_ortho(_w * _zoom, _h * _zoom, 1.0, 32000.0);
	camera_set_proj_mat(_camera, _projmat);
#endregion