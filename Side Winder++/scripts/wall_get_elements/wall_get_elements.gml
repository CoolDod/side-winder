#region SCRIPT DESCRIPTION
	/// @func tile_get_elements(index)
	/// @desc Returns an array of the elements in the given tile
	/// @arg {int} index 
#endregion
#region DECLARE VARIABLES
	var _index = argument0;
#endregion


#region TILE GET ELEMENTS
	var _elements = 0;

	// Loop through list of all tile elements
	for (var _i = 0; _i < array_height_2d(global.tileElements); ++_i) {
		var _array = global.tileElements[_i];
		
		// Loop through all tile indexes in tile element
		for (var _j = 0; _j < array_length_1d(_array); ++_j) {
			// If given tile index has the current tile element
			if (_index == _array[_j]) {
				_elements = array_add(_elements, [_array]);
				
				break;
			}
		}
	}
	
	return _elements;
#endregion