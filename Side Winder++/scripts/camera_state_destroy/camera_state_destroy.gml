#region SCRIPT DESCRIPTION
	/// @func camera_state_destroy(id)
	/// @desc Destroys the given camera state and removes it from memory
	/// @arg {int} id The id of the camera to destroy
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
#endregion


#region CAMERA STATE DESTROY
	ds_map_destroy(_id);
#endregion