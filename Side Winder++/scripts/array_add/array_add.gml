#region SCRIPT DESCRIPTION
	/// @func array_add(id,[values])
	/// @desc Adds the given values to the end of the given array
	/// @arg {int} id The id of the array
	/// @arg {array} [values] The values to add
#endregion
#region DECLARE VARIABLES
	var _id = argument0;
	var _values = argument1;
#endregion


#region ARRAY ADD
	var _len = array_length_1d(_id);

	for (var _i = 0; _i < array_length_1d(_values); ++_i)
		_id[_len + _i] = _values[_i];
		
	return _id;
#endregion