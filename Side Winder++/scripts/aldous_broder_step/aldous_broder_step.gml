#region SCRIPT DESCRIPTION
	/// @func aldous_broder_step()
	/// @desc Runs a single step of the aldous-broder algorithm
#endregion


#region ALDOUS BRODER STEP
	// Get neighbours
	var _neighbours = cell_get_neighbours(ab_xindex, ab_yindex);
	
	// Get available directions
	var _directions = array_create(0);
	
	if (ab_yindex > 0)
		_directions = array_add(_directions, [Directions.North]);
	if (ab_yindex < oMaze.ycells - 1)
		_directions = array_add(_directions, [Directions.South]);
	if (ab_xindex > 0)
		_directions = array_add(_directions, [Directions.West]);
	if (ab_xindex < oMaze.xcells - 1)
		_directions = array_add(_directions, [Directions.East]);
		
	// Choose direction
	var _dir = _directions[irandom(array_length_1d(_directions) - 1)];
	
	// Move to new cell
	// If chosen direction is up
	if (_dir == Directions.North) {
		--ab_yindex;
		
		var _cell = oMaze.cell_map[ab_xindex, ab_yindex];
		
		// If cell hasn't been visited before
		if (_cell == noone) {
			oMaze.cell_map[ab_xindex, ab_yindex] = cell_create(ab_xindex, ab_yindex, false, true, false, false);
			++cell_num;
		}
	}
	// If chosen direction is down
	else if (_dir == Directions.South) {
		++ab_yindex;
		
		var _cell = oMaze.cell_map[ab_xindex, ab_yindex];
		
		// If cell hasn't been visited before
		if (_cell == noone) {
			oMaze.cell_map[ab_xindex, ab_yindex] = cell_create(ab_xindex, ab_yindex, true, false, false, false);
			++cell_num;
		}
	}
	// If chosen direction is left
	else if (_dir == Directions.West) {
		--ab_xindex;
		
		var _cell = oMaze.cell_map[ab_xindex, ab_yindex];
		
		// If cell hasn't been visited before
		if (_cell == noone) {
			oMaze.cell_map[ab_xindex, ab_yindex] = cell_create(ab_xindex, ab_yindex, false, false, false, true);
			++cell_num;
		}
	}
	// If chosen direction is right
	else if (_dir == Directions.East) {
		++ab_xindex;
		
		var _cell = oMaze.cell_map[ab_xindex, ab_yindex];
		
		// If cell hasn't been visited before
		if (_cell == noone) {
			oMaze.cell_map[ab_xindex, ab_yindex] = cell_create(ab_xindex, ab_yindex, false, false, true, false);
			++cell_num;
		}
	}
#endregion
