#region SCRIPT DESCRIPTION
	/// @func wall_autotile_add(x,y)
	/// @desc Checks and modifies all neighbouring walls
	/// @arg {int} x
	/// @arg {int} y
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
#endregion


#region WALL AUTOTILE ADD
	// Create wall
	var _inst = instance_create_layer(_x, _y, "iWalls", oWall);

	// Get neighbouring walls
	var _neighbours = wall_get_neighbours(_inst);
	
	var _top = _neighbours[Directions.North];
	var _bottom = _neighbours[Directions.South];
	var _left = _neighbours[Directions.West];
	var _right = _neighbours[Directions.East];
	var _topleft = _neighbours[Directions.NorthWest];
	var _topright = _neighbours[Directions.NorthEast];
	var _bottomleft = _neighbours[Directions.SouthWest];
	var _bottomright = _neighbours[Directions.SouthEast];
	
	// Change top left
	// If there is a wall at the top left
	if (_topleft != noone) {
		// If wall has a corner at the bottom right, remove it
		if (array_contains(_topleft.elements, TileElement.cornerBR))
			wall_remove_element(_topleft, TileElement.cornerBR);
	}
	
	// Change top right
	// If there is a wall at the top right
	if (_topright != noone) {
		// If wall has corner at the bottom left, remove it
		if (array_contains(_topright.elements, TileElement.cornerBL))
			wall_remove_element(_topright, TileElement.cornerBL);
	}
	
	// Change bottom left
	// If there is a wall at the bottom left
	if (_bottomleft != noone) {
		// If wall has corner at the top right, remove it
		if (array_contains(_bottomleft.elements, TileElement.cornerTR))
			wall_remove_element(_bottomleft, TileElement.cornerTR);
	}
	
	// Change bottom right
	// If there is a wall at the bottom right
	if (_bottomright != noone) {
		// If wall has a corner at the top left, remove it
		if (array_contains(_bottomright.elements, TileElement.cornerTL))
			wall_remove_element(_bottomright, TileElement.cornerTL);
	}
	
	// Change top
	// If there is a wall at the top
	if (_top != noone) {
		// Remove bottom side
		_top.elements = array_subtract(_top.elements, [TileElement.sideB]);
		
		// If there is no wall to the left and there is a wall to the top left
		if (_left == noone) {
			if (_topleft != noone)
				_top.elements = array_add(_top.elements, [TileElement.cornerBL]);
		}
		else if (_topleft == noone)
			_inst.elements = array_add(_inst.elements, [TileElement.cornerTL]);
			
		// If there is no wall to the right and there is a wall to the top right
		if (_right == noone) {
			if (_topright != noone)
				_top.elements = array_add(_top.elements, [TileElement.cornerBR]);
		}
		else if (_topright == noone)
			_inst.elements = array_add(_inst.elements, [TileElement.cornerTR]);

		_top.image_index = wall_elements_get_index(_top.elements);
	}
	// If there is no wall on the top add top side element
	else
		_inst.elements = array_add(_inst.elements, [TileElement.sideT]);
	
	// Change bottom
	// If there is a wall at the bottom
	if (_bottom != noone) {
		// Remove top side from bottom wall
		_bottom.elements = array_subtract(_bottom.elements, [TileElement.sideT]);
		
		// If wall doesn't have a side on the left and there is no wall to the left
		if (_left == noone) {
			if (_bottomleft != noone)
				_bottom.elements = array_add(_bottom.elements, [TileElement.cornerTL]);
		}
		else if (_bottomleft == noone)
			_inst.elements = array_add(_inst.elements, [TileElement.cornerBL]);
		
		// If wall doesn't have a side on the right and there is no wall to the right
		if (_right == noone) {
			if (_bottomright != noone)
				_bottom.elements = array_add(_bottom.elements, [TileElement.cornerTR]);
		}
		else if (_bottomright == noone)
			_inst.elements = array_add(_inst.elements, [TileElement.cornerBR]);
		
		_bottom.image_index = wall_elements_get_index(_bottom.elements);
	}
	// If there is no wall on the bottom add bottom side element
	else
		_inst.elements = array_add(_inst.elements, [TileElement.sideB]);
	
	// Change left
	// If there is a wall at the left
	if (_left != noone) {
		// Remove right side of left wall 
		_left.elements = array_subtract(_left.elements, [TileElement.sideR]);
		
		// If wall doesn't have a side on the top and there is no wall to the top
		if (_top == noone && _topleft != noone)
			_left.elements = array_add(_left.elements, [TileElement.cornerTR]);
			
		// If wall doesn't have a side on the bottom and there is no wall to the bottom
		if (_bottom == noone && _bottomleft != noone)
			_left.elements = array_add(_left.elements, [TileElement.cornerBR]);
			
		_left.image_index = wall_elements_get_index(_left.elements);
	}
	// If there is no wall on the left add left side element
	else
		_inst.elements = array_add(_inst.elements, [TileElement.sideL]);
	
	// Change right
	// If there is a wall at the right
	if (_right != noone) {
		// Remove left side of right wall
		_right.elements = array_subtract(_right.elements, [TileElement.sideL]);
		
		// If wall doesn't have a side on the top and there is no wall to the top
		if (_top == noone && _topright != noone)
			_right.elements = array_add(_right.elements, [TileElement.cornerTL]);
			
		// If wall doesn't have a side on the bottom and there is no wall to the bottom
		if (_bottom == noone && _bottomright != noone)
			_right.elements = array_add(_right.elements, [TileElement.cornerBL]);
		
		_right.image_index = wall_elements_get_index(_right.elements);
	}
	// If there no wall on the right add right side element
	else
		_inst.elements = array_add(_inst.elements, [TileElement.sideR]);
		
	// Set new image index for wall
	_inst.image_index = wall_elements_get_index(_inst.elements);
	
	return (_inst);
#endregion