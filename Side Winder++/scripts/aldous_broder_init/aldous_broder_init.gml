#region SCRIPT DESCRIPTION
	/// @func aldous_broder_init()
	/// @desc Initialises the aldous broder algorithm
#endregion


#region ALDOUS BRODER INIT
	oMaze.maze_generating = true;
	cell_num = 0;

	ab_xindex = irandom(oMaze.xcells - 1);
	ab_yindex = irandom(oMaze.ycells - 1);
	
	maze_set_start(ab_xindex, ab_yindex);
	
	cell_create(ab_xindex, ab_yindex, false, false, false, false);
#endregion