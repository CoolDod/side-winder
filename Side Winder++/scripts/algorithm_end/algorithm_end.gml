#region SCRIPT DESCRIPTION
	/// @func algorithm_end()
	/// @desc Finishes the maze by placing any necessary objects
#endregion


#region ALGORITHM END
	// Create player
	instance_create_layer(oMaze.mx + oMaze.cell_width / 2, oMaze.my + oMaze.cell_height / 2, "iPlayer", oPlayer);
	
	// Set maze generating to false
	oMaze.maze_generating = false;
	
	if (oMaze.generate_fast) {
		// Create camera
		var inst = instance_create_layer(room_width / 2, room_height / 2, "iControllers", oCamera);
		camera_state_set(inst, 1, true);
	}
	else
		// Set camera mode
		camera_state_set(oCamera, 1, true);
#endregion