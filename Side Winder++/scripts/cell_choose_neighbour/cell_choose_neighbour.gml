#region SCRIPT DESCRIPTION
	/// @func cell_choose_neighbour(neighbours)
	/// @desc Randomly chooses a neighbour from the given list of neighbours and returns it
	/// @arg {array} neighbours The array of neighbouring cells
#endregion
#region DECLARE VARIABLES	
	var _neighbours = argument0;
#endregion


#region CELL CHOOSE NEIGHBOUR
	var _list = ds_list_create();
	
	var _i = 0;
	repeat (4) {
		if (_neighbours[_i] != noone)
			ds_list_add(_list, _neighbours[_i]);
		
		++_i;
	}
	
	var _cell = _list[| irandom(ds_list_size(_list) - 1)];
	ds_list_destroy(_list);
	
	return _cell;
#endregion