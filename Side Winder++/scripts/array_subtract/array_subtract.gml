#region SCRIPT DESCRIPTION
	/// @func array_subtract(array1,array2)
	/// @desc Returns the contents of the first array with the contents of the second array removed
	/// @arg {int} array1 The array to subtract from
	/// @arg {int} array2 The array to subtract
#endregion
#region DECLARE VARIABLES
	var _a1 = argument0;
	var _a2 = argument1;
#endregion


#region ARRAY SUBTRACT
	var _newArray = array_create(0);
	var _addItem = true;
	
	for (var _i = 0; _i < array_length_1d(_a1); ++_i) {
		_addItem = true;
		
		for (var _j = 0; _j < array_length_1d(_a2); ++_j) {
			if (_a1[_i] == _a2[_j])
				_addItem = false;
		}
		
		if (_addItem)
			_newArray[array_length_1d(_newArray)] = _a1[_i];
	}
	
	return _newArray;
#endregion