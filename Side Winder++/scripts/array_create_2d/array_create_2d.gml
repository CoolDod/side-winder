#region SCRIPT DESCRIPTION
	/// @func array_create_2d(xsize,ysize,[value])
	/// @desc Creates and returns an array of the given size
	/// @arg {int} xsize The size of the x dimension of the array
	/// @arg {int} ysize The size of the y dimension of the array
	/// @arg {optional} [value] The value to set all array entries to
#endregion
#region DECLARE VARIABLES
	var _xsize = argument[0];
	var _ysize = argument[1];
	
	if (argument_count > 2)
		var _val = argument[2];
	else
		var _val = 0;
#endregion


#region ARRAY CREATE 2D
	var _array;

	var _i = 0;
	repeat (_xsize) {
		var _j = 0;
		repeat (_ysize){
			_array[_i,_j] = _val;
			
			++_j;
		}
		
		++_i;
	}
	
	return _array;
#endregion