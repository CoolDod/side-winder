#region SCRIPT DESCRIPTION
	/// @func camera_setup(x,y,width,height,rot,zoom,view,visible)
	/// @desc Creates and sets up a camera
	/// @arg {real} x
	/// @arg {real} y
	/// @arg {real} width
	/// @arg {real} height
	/// @arg {real} rot
	/// @arg {real} zoom
	/// @arg {int} view
	/// @arg {bool} visible
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
	var _w = argument2;
	var _h = argument3;
	var _rot = argument4;
	var _zoom = argument5;
	var _view = argument6;
	var _visible = argument7;
#endregion


#region CAMERA SETUP
	var _camera = camera_create();
	
	if (!view_enabled)
		view_enabled = true;
	
	view_visible[_view] = _visible;
	view_set_camera(_view, _camera);
	
	var _projmat = matrix_build_projection_ortho(_w * _zoom, _h * _zoom, 1.0, 32000.0);
	var _viewmat = matrix_build_lookat(_x, _y, -10, _x, _y, 0, dsin(_rot), dcos(_rot), 0);
	
	camera_set_proj_mat(_camera, _projmat);
	camera_set_view_mat(_camera, _viewmat);
	
	return (_camera);
#endregion