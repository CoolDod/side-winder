#region SCRIPT DESCRIPTION
	/// @func cell_create(x,y,connected_up,connected_down,connected_left,connected_right)
	/// @desc Creates and returns a cell data structure
	/// @arg {int} x The x index of the cell
	/// @arg {int} y The y index of the cell
	/// @arg {bool} connected_up Whether or not the cell is connected to the cell above it
	/// @arg {bool} connected_down Whether or not the cell is connected to the cell below it
	/// @arg {bool} connected_left Whether or not the cell is connected to the cell to the left
	/// @arg {bool} connected_right Whether or not the cell is connected to the cell to the right
#endregion
#region DECLARE VARIABLES
	var _x = argument0;
	var _y = argument1;
	var _connected_up = argument2;
	var _connected_down = argument3;
	var _connected_left = argument4;
	var _connected_right = argument5;
#endregion


#region CELL CREATE
	// Variables
	var _wall_width = oMaze.wall_width;
	var _wall_height = oMaze.wall_height;
	
	var _wall_half_width = _wall_width / 2;
	var _wall_half_height = _wall_height / 2;
	
	var _cx = _x * oMaze.cell_width + oMaze.mx;
	var _cy = _y * oMaze.cell_height + oMaze.my;
	
	var _x1 = _cx - _wall_half_width;
	var _y1 = _cy - _wall_half_height;
	var _x2 = _cx + oMaze.cell_width - _wall_half_width;
	var _y2 = _cy + oMaze.cell_height - _wall_half_height;
	
	// Change tile
	var _i = 0;
	repeat (oMaze.xwalls) {
		var _j = 0;
		repeat (oMaze.ywalls) {
			tilemap_set_at_pixel(oMaze.tile_layer, 1, _x1 + _i * _wall_width, _y1 + _j * _wall_height);
			
			++_j;
		}
	 
	 ++_i;
	}
	
	// Neighbours
	var _neighbours = cell_get_neighbours(_x, _y);
	
	// Corners
	if (!position_meeting(_x1 + _wall_half_width, _y1 + _wall_half_height, oWall))
		wall_autotile_add(_x1, _y1);
	if (!position_meeting(_x2 + _wall_half_width, _y1 + _wall_half_height, oWall))
		wall_autotile_add(_x2, _y1);
	if (!position_meeting(_x1 + _wall_half_width, _y2 + _wall_half_height, oWall))
		wall_autotile_add(_x1, _y2);
	if (!position_meeting(_x2 + _wall_half_width, _y2 + _wall_half_height, oWall))
		wall_autotile_add(_x2, _y2);
	
	// Top side
	if (!_connected_up || _neighbours[0] == noone) {
		for (var _i = 1; _i < oMaze.xwalls; _i++) {
			var _wx = _x1 + _wall_width * _i;
			
			if (!place_meeting(_wx + _wall_half_width, _y1 + _wall_half_height, oWall))
				wall_autotile_add(_wx, _y1);
		}
	}
	else if (_connected_up && _neighbours[0] != noone) {
		var _cell = oMaze.cell_map[_x,_y-1];
		_cell[Directions.North] = true;
		oMaze.cell_map[_x,_y-1] = _cell;
		
		for (var _i = 1; _i < oMaze.xwalls; _i++)
			wall_autotile_remove(_x1 + _wall_width * _i, _y1);
	}
	
	// Bottom side
	if (!_connected_down || _neighbours[1] == noone) {
		for (var _i = 1; _i < oMaze.xwalls; _i++) {
			var _wx = _x1 + _wall_width * _i;
			
			if (!place_meeting(_wx + _wall_half_width, _y2 + _wall_half_height, oWall))
				wall_autotile_add(_wx, _y2);
		}
	}
	else if (_connected_down && _neighbours[1] != noone) {
		var _cell = oMaze.cell_map[_x,_y+1];
		_cell[Directions.South] = true;
		oMaze.cell_map[_x,_y+1] = _cell;
		
		for (var _i = 1; _i < oMaze.xwalls; _i++)
			wall_autotile_remove(_x1 + _wall_width * _i, _y2);
	}
	
	// Left side
	if (!_connected_left || _neighbours[2] == noone) {
		for (var _i = 1; _i < oMaze.ywalls; _i++) {
			var _wy = _y1 + _wall_height * _i;
			
			if (!place_meeting(_x1 + _wall_half_width, _wy + _wall_half_height, oWall))
				wall_autotile_add(_x1, _wy);
		}
	}
	else if (_connected_left && _neighbours[2] != noone) {
		var _cell = oMaze.cell_map[_x-1,_y];
		_cell[Directions.West] = true;
		oMaze.cell_map[_x-1,_y] = _cell;
		
		for (var _i = 1; _i < oMaze.ywalls; _i++)
			wall_autotile_remove(_x1, _y1 + _wall_height * _i);
	}
	
	// Right side
	if (!_connected_right || _neighbours[3] == noone) {
		for (var _i = 1; _i < oMaze.ywalls; _i++) {
			var _wy = _y1 + _wall_height * _i;
			
			if (!place_meeting(_x2 + _wall_half_width, _wy + _wall_half_height, oWall))
				wall_autotile_add(_x2, _wy);	
		}
	}
	else if (_connected_right && _neighbours[3] != noone) {
		var _cell = oMaze.cell_map[_x+1,_y];
		_cell[Directions.East] = true;
		oMaze.cell_map[_x+1,_y] = _cell;
		
		for (var _i = 1; _i < oMaze.ywalls; _i++)
			wall_autotile_remove(_x2, _y1 + _wall_height * _i);
	}
	
	// Data structure
	var _cell;
	
	_cell[0] = _x;
	_cell[1] = _y;
	_cell[Directions.North + 2] = _connected_up;
	_cell[Directions.South + 2] = _connected_down;
	_cell[Directions.West + 2] = _connected_left;
	_cell[Directions.East + 2] = _connected_right;
	
	oMaze.cell_map[_x,_y] = _cell;
	
	return (_cell);
#endregion
